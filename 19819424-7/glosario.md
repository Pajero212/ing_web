a.Control de versiones (VC)
         Es un sistema encargado de almacenar versiones de códigos, de manera que funcione como un tipo de respaldo para poder trabajar con él en caso de realizar cambios a futuro, modificando en gran parte el contexto del código. Además, ofrece otras utilidades, tales como fusionar los códigos que se están trabajando, y permite la interacción entre un grupo de trabajo remoto, para poder trabajar individualmente.

         (https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)
b.Control de versiones distribuido (DVC)
         Permite a los trabajadores de un mismo grupo de trabajo, poder almacenar y sincronizar su código en un repositorio central, el cual sincroniza la copia de todos los cambios efectuados de manera aislada, fusionando las variantes de versiones de cada uno de los integrantes. Esto permite en parte un respaldo en caso de futura pérdida, lo cual permite al equipo poder seguir trabajando con la última versión de dicho segmento.

         (https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones)
c.Repositorio remoto y Repositorio local
    Fuente centralizada de almacenamiento de información, en este caso se diferencia entre remoto y local, teniendo el primero lugar en el ordenador del usuario, en el siguiente caso la necesidad de almacenar una mayor cantidad de información y de versiones de los trabajos eficientemente, sustenta la funcionalidad principal del repositorio remoto, que permite almacenar en algún punto de la red, dichos archivos.

    (https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales/)
d.Copia de trabajo / Working Copy
    Copia de una versión del código previo, el cual se extrae de la base de datos (repositorio remoto), para poder copiados en el repositorio local para poder trabajar con esta versión.

    (https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git)
e.Área de Preparación / Staging Area
    Es un archivo que almacena las variantes en el código, puede ser considerado como un tipo índice del total de códigos, se almacena la información de los datos modificados para posteriormente poder fusionar esta versión con la antigua almacenada en el repositorio remoto.

    (https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git)
f.Preparar Cambios / Stage Changes
    Estado en el cual se marca un archivo modificado en su versión actual, para poder ser modificado en el repositorio remoto.

    (https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git)
g.Confirmar cambios / Commit Changes
    Estado en el cual se confirma que los datos ingresados respecto a los cambios en el código son aceptados.

    (https://git-scm.com/book/es/v1/Empezando-Fundamentos-de-Git)
h.Commit
    Comando, en el cual se confirma los cambios realizados. Además permite almacenar dicho cambio conjunto a un mensaje, el cual generalmente se utiliza para describir los cambios realizados.

    (https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)
i.clone
    Comando, el cual se utiliza para clonar un repositorio existente a otro repositorio, en este caso se copian la mayoría de los archivos almacenados.

    (https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)
j.pull
    Comando, el cual permite fusionar y recuperar una rama remota con la rama actual (dentro del control de versiones de un trabajo).

    (https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)
k.push
    Comando, permite compartir un proyecto de un repositorio a otro remoto en donde se debe especificar la rama (master) y el nombre del servidor de origen.

    (https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)
l.fetch
    Comando, se utiliza para descargar los cambios realizados en el repositorio remoto al repositorio local.

    (https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)
m.merge
    Comando, permite poder generar modificaciones en la rama que se especifique en dicho comando.
n.status
    Comando, el cual ofrece información respecto al estado del repositorio actual, permite poder realizar seguimiento a las distintas operaciones realizadas.

    (https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio)
o.log
    Comando, permite al usuario visualizar información sobre las versiones y modificaciones realizadas anteriormente en el archivo, además ofrece una cantidad de opciones para poder especificar de mejor manera la información respecto a la modificación deseada.

    (https://git-scm.com/book/es/v1/Fundamentos-de-Git-Viendo-el-hist%C3%B3rico-de-confirmaciones)
p.checkout
    Comando, en conjunto con una serie de opciones, ofrece la versatilidad de moverse de rama en rama según se desee, especificando en estos casos los nombres de dichas ramas
q.Rama / Branch
    Apuntador móvil a los distintos estados de confirmaciones, lo cual permite poder moverse a través de las distintas versiones anteriores del proyecto, en este caso se desplaza dicho apuntador por los estados anteriores, a posterior de realizar el comando commit.

    (https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F)
r.Etiqueta / Tag
    Se utiliza para poder dar una especificación respecto a la versión de los archivos almacenados, en este caso se agregan para poder manejar de mejor manera las distintas versiones y actualizaciones de un trabajo.

    (https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas)
