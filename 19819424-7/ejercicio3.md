a. Cantidad y nombre de los branches

Cantidad: Se pueden identificar de dos tipos, principales en donde se encuentran las ramas "master" y "develop", por el otro lado también se encuentran las ramas de apoyo las cuales son "features", "release" y "hotfixes".

  -Master: rama principal del código fuente HEAD, en esta se almacena aquellos segmentos que están listos o en producción para poder ser subidos posteriormente.

  -Develop: rama principal en donde el código fuente HEAD almacena el conjunto general de cambios realizados hasta la ultima modificacion del código (también es llamada "rama de integración").

  -Features: se utiliza principalmente para efectuar cambios y nuevas caracteristicas para una nueva versión del código. En este caso no se pasa a el origin, y se ramnifica a partir de la rama develop, para luego volver a fusionarse con esta. Especialmente se usa para agregar nuevas funcionalidades a un objeto en particular.

  -Release: su uso se limita a corregir pequeñas inperfecciones en el código, se genera a partir de la fusión entre la rama develop y la master, generando una versión específica para poder realizar los cambios, una vez finalizado se específica la versión actual mediante una etiqueta, y luego se fusiona finalmente con la rama Master con la nueva versión.

  -Hotfixes: se utiliza para generar una solución en caso de que se produzcan bugs en la ejecución de un código, se origina a partir de la rama Master y no limita la ejecución en un equipo de trabajo, finalmente se termina fusionando con las ramas master y develop.

  (https://jesuslc.com/2012/11/24/una-buena-manera-de-afrontar-la-ramificacion-branching-en-git/)
b. Cantidad y nombre de las etiquetas

Cantidad: Se pueden identificar de tres tipos, etiquetas "anotadas", "ligeras" y "tardío".

  -Etiquetas Anotadas: en este tipo de etiqueta se espeficia mediante el commit la versión la cual será almacenada, en este caso el usuario incluye la especificación deseada.

  -Etiquetas ligeras: en este caso no es necesario agregar una especificación de la versión del código, además si se ejecuta el comando "git show", sólo se mostrará el commit.

  -Etiquetado tardío: en este caso se realiza para especificar un etiquetado de una versión ya modificada y almacenada, en la que en primera instancia no se hizo.

  (https://git-scm.com/book/es/v2/Fundamentos-de-Git-Etiquetado)

c. Nombre, mensaje y código hash de los últimos 3 commits.

commit 8919c7b94542d6099ffdcfbfbaa9e7a6f8f81948 (HEAD -> master, origin/master)
Author: Pajero212 <ayrton.mera.c@mail.pucv.cl>
Date:   Sat Mar 30 10:41:58 2019 -0300

  ejercicio glosario taller

commit a734b2e25dbd97a658559e619c21321bd1769b8d
Author: Pajero212 <ayrton.mera.c@mail.pucv.cl>
Date:   Sat Mar 30 02:02:46 2019 -0300

  datos personales y de cuenta

commit 9ce05291a23f3814e9112c1b1d2a787a6c5deca8
Author: Pajero212 <ayrton.mera.c@mail.pucv.cl>
Date:   Sat Mar 30 01:52:26 2019 -0300

  ejercicio glosario taller

commit ac9e72e8150e0a48551a4e4cbabd7b0ca65975bd
Author: Pajero212 <ayrton.mera.c@mail.pucv.cl>
Date:   Sat Mar 30 01:06:02 2019 -0300

  first commit
